import React from 'react'
import  axios  from 'axios'
import { Table, Button, Modal, ModalFooter, ModalHeader, ModalBody, FormGroup, Input, Label } from 'reactstrap'

class App extends React.Component {
  state={
    books:[],
    newBookData: {
      title:'',
      price:'',
    },
    editBookData: {
      id:'',
      title:'',
      price:''
    },
    newBookModal : false
  }

  componentWillMount(){
      axios.get('http://localhost:3000/books').then((response)=>{
        this.setState({
          books: response.data
        })
      });
  }
  toggleNewBookModel(){
    this.setState({
      newBookModal : !this.state.newBookModal
    });
    //this.state.newBookModal = true; 
  }
  toggleEditBookModal() {
    this.setState({
      editBookModal: ! this.state.editBookModal
    });
  }

  addBook(){
    axios.post('http://localhost:3000/books',this.state.newBookData).then((response)=>{
      let {books} = this.state;
      
      books.push(response.data);
      this.setState({books, newBookModal:false, newBookData: {
        title: '',
        price: ''
      }})
    });
  };

  updateBook() {
    let { title, price } = this.state.editBookData;

    axios.put('http://localhost:3000/books/' + this.state.editBookData.id, {
      title, price
    }).then((response) => {
      this._refreshBooks();

      this.setState({
        editBookModal: false, editBookData: { id: '', title: '', price: '' }
      })
    });
  }
  editBook(id,title,price){
    this.setState({
      editBookData: { id, title, price }, editBookModal: ! this.state.editBookModal
    });
  };

  deleteBook(id) {
    axios.delete('http://localhost:3000/books/' + id).then((response) => {
      this._refreshBooks();
    });
  }

  _refreshBooks() {
    axios.get('http://localhost:3000/books').then((response) => {
      this.setState({
        books: response.data
      })
    });
  }

  render(){
    let books =this.state.books.map((book)=>{
      return(
        <tr key={book.id}>
          <td>{book.id}</td>
          <td>{book.title}</td>
          <td>Rs.{book.price}</td>
          <td>
            <Button color="success" size="sm" className="me-2" onClick={this.editBook.bind(this, book.id, book.title, book.price)}>Edit</Button>
            <Button color="danger" size="sm" onClick={this.deleteBook.bind(this,book.id)}>Delete</Button>
          </td>
        </tr>
      )
    })
    return (
      <div className="App container mt-4">
        <div className="d-flex justify-content-between">
            <h1 className="fs-3 text-capitalize fw-bold text-decoration-underline">Book Store</h1>
            <Button color="primary" onClick={this.toggleNewBookModel.bind(this)}>Add Book</Button>
        </div>
        <Modal isOpen={this.state.newBookModal} toggle={this.toggleNewBookModel} >
          <ModalHeader toggle={this.toggleNewBookModel.bind(this)}>Add a New Book</ModalHeader>
          <ModalBody>
            <FormGroup>
                <Label for="title">Title</Label>
                <Input type="text" name="title" id="title" 
                    value={this.state.newBookData.title} 
                    onChange={(e)=>{
                      let {newBookData} = this.state;
                      newBookData.title = e.target.value;
                      this.setState({newBookData});
                    }}
                />
            </FormGroup>
            <FormGroup>
                <Label for="price">Price</Label>
                <Input type="number" name="price" id="price" 
                    value={this.state.newBookData.price}
                    onChange={(e)=>{
                      let {newBookData} = this.state;
                      newBookData.price = e.target.value;
                      this.setState({newBookData});
                    }}
                />
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.addBook.bind(this)}>Add Book</Button>{' '}
            <Button color="secondary" onClick={this.toggleNewBookModel.bind(this)}>Cancel</Button>
          </ModalFooter>
        </Modal>
        <Modal isOpen={this.state.editBookModal} toggle={this.toggleEditBookModal.bind(this)}>
            <ModalHeader toggle={this.toggleEditBookModal.bind(this)}>Edit a new book</ModalHeader>
            <ModalBody>
              <FormGroup>
                <Label for="title">Title</Label>
                <Input id="title" value={this.state.editBookData.title} onChange={(e) => {
                  let { editBookData } = this.state;

                  editBookData.title = e.target.value;

                  this.setState({ editBookData });
                }} />
              </FormGroup>
              <FormGroup>
                <Label for="price">Price</Label>
                <Input id="price" value={this.state.editBookData.price} onChange={(e) => {
                  let { editBookData } = this.state;

                  editBookData.price = e.target.value;

                  this.setState({ editBookData });
                }} />
              </FormGroup>

            </ModalBody>
            <ModalFooter>
              <Button color="primary" onClick={this.updateBook.bind(this)}>Update Book</Button>{' '}
              <Button color="secondary" onClick={this.toggleEditBookModal.bind(this)}>Cancel</Button>
            </ModalFooter>
        </Modal>
        <Table className="table mt-4">
          <thead className="bg-dark text-white">
            <tr>
              <th>S.N</th>
              <th>Book Name</th>
              <th>Price</th>
              <th>Action</th>
            </tr>
          </thead>
  
          <tbody>
            {books}
          </tbody>
        </Table>
      </div>
    );
  }
}

export default App;
